Product {

    Group{
        name: "Code"
        prefix: "fk"
        excludeFiles: "/**/internal/**/*"
        files: [
            "/**/*.html",
            "/**/*.jinja2",
            "/**/*.py",
            "/**/*.txt",
        ]
    }
    Group{
        name: "Test"
        prefix: "tests"
        excludeFiles: "/**/internal/**/*"
        files: [
            "/**/*.html",
            "/**/*.jinja2",
            "/**/*.py",
            "/**/*.txt",
        ]
    }
    Group{
        name: "Meta"
        prefix: "./"
        excludeFiles: "/**/internal/**/*"
        files: [
            "*.qbs",
            ".*ignore",
            ".gitlab*",
            "Dockerfile",
            "Makefile",
            "README.md",
            "VERSION",
            "design/*.svg",
            "requirements/*",
            "docker-*.yaml",
            "local-*.sh",
            "setup.*",
            "*.conf",
            "*.py",
            ".env",
        ]
    }
    Group{
        name: "Design"
        prefix: "design"
        excludeFiles: "/**/internal/**/*"
        files: [
            "/**/*.jpeg",
            "/**/*.json",
            "/**/*.png",
            "/**/*.svg",
        ]
    }
}
